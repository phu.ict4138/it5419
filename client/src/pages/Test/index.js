import { useKeycloak } from "@react-keycloak/web"
import { Link } from "react-router-dom"

const UserInfo = () => {

    const { keycloak } = useKeycloak()
    const data = keycloak.tokenParsed

    const roles_ = [
        "quan-ly-cua-hang",
        "nhan-vien",
        "thu-ngan",
        "giam-doc",
    ]

    return (
        <div className="w-full flex flex-col border-b border-t pb-3 text-sm items-center">
            <span className="font-semibold text-2xl py-2">UserInfo</span>
            <div className="w-96 flex flex-row items-center mb-2">
                <div className="w-32">User ID (sub):</div>
                <div className="w-full">{data.sub}</div>
            </div>
            <div className="w-96 flex flex-row mb-2">
                <div className="w-32">Name:</div>
                <div className="w-full">{data.name}</div>
            </div>
            <div className="w-96 flex flex-row mb-2">
                <div className="w-32">Username:</div>
                <div className="w-full">{data.preferred_username}</div>
            </div>
            <div className="w-96 flex flex-row mb-2">
                <div className="w-32">Email:</div>
                <div className="w-full">{data.email}</div>
            </div>
            <div className="w-96 flex flex-row mb-2">
                <div className="w-32">Roles:</div>
                <div className="w-full">{data.realm_access.roles.filter(i => roles_.includes(i)).join(", ") ?? "Chưa có role"}</div>
            </div>
            <span>Mở console</span>
        </div>
    )
}

const CreateNewUser = () => {

    const { keycloak } = useKeycloak()
    const data = keycloak.tokenParsed

    return (
        <div className="w-full flex flex-col border-b pb-3 text-sm items-center">
            <span className="font-semibold text-2xl py-2">Create New User</span>

        </div>
    )
}

export default () => {

    const { keycloak } = useKeycloak()

    return (
        <div className="w-full min-h-screen flex flex-col items-center">
            <div className="text-center font-semibold text-3xl my-3">Test</div>
            <div className="flex flex-col items-center my-3">
                <button
                    className="w-24 h-8 border rounded font-semibold text-xs"
                    onClick={async () => await keycloak.logout({ redirectUri: "http://localhost:3000/login" })}>
                    Logout
                </button>
            </div>
            <UserInfo />
            {/* <CreateNewUser /> */}
            <div className="flex flex-col">
                <div>Tài khoản có role giám đốc:</div>
                <div>Username: giamdoc</div>
                <div>Password: 123456</div>
            </div>

            <div className="flex flex-col items-center mt-3">
                <Link to="/dashboard">
                    <button className="w-24 h-8 border rounded font-semibold text-xs">
                        Dashboard
                    </button>
                </Link>
            </div>
            <div className="w-full flex flex-col pl-20">
                <span>***Admin***</span>
                <span>- Truy cập <a target="_blank" className="text-blue-500 hover:opacity-70" href="http://3.0.145.150">http://3.0.145.150</a></span>
                <span>- Đăng nhập: username: admin, password: 123456</span>
            </div>
            <div className="w-full flex flex-col pl-20 mt-5">
                <span>***Lấy public key decode token***</span>
                <span>- Truy cập <a target="_blank" className="text-blue-500 hover:opacity-70" href="http://3.0.145.150/admin/master/console/#/sso/realm-settings/keys">http://3.0.145.150/admin/master/console/#/sso/realm-settings/keys</a></span>
                <span>- Đăng nhập: username: admin, password: 123456</span>
                <span>- Bấm "Public key" của thuật toán RS256</span>
                <span>- Xem file: verifyToken.js</span>
            </div>
        </div>
    )
}