import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import { ReactKeycloakProvider } from "@react-keycloak/web";
import Keycloak from "keycloak-js";
import { KEYCLOAK_URL } from "./config"
import Loading from "./components/Loading";

const authClient = new Keycloak({
    url: KEYCLOAK_URL,
    realm: "sso",
    clientId: "ban-hang",
});

ReactDOM.render(
    <Provider store={store}>
        {/* <React.StrictMode> */}
        <ReactKeycloakProvider authClient={authClient} LoadingComponent={<Loading />}>
            <App />
        </ReactKeycloakProvider>
        {/* </React.StrictMode> */}
    </Provider>,
    document.getElementById("root")
);
